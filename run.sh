#!/bin/sh

# The QEMU binary may have a different name on your system
qemu-system-x86_64 -no-reboot -vga cirrus -drive file=disk.img,index=0,media=disk,format=raw -serial file:debug.txt
