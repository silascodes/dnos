
#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>
#include "kernel/arch.h"
#include "kernel/kstdlib.h"
#include "kernel/kstring.h"

extern void _x86_dbg_putchar(const char pChar);
extern void _x86_dbg_puts(const char *pStr);

void arch_dbg_printf(const char *pFormat, ...) {
    char buf[32];
    va_list args;

    kmemset(buf, 0, 32);
    va_start(args, pFormat);

    char *fmt = (char*)pFormat;
    while(*fmt != 0) {
        if(*fmt == '%' && *(fmt + 1) != 0) {
            fmt++;
            switch(*fmt) {
                case '%':
                    _x86_dbg_putchar('%');
                    break;

                case 'c':
                    _x86_dbg_putchar((char)va_arg(args, int32_t));
                    break;

                case 's':
                    _x86_dbg_puts(va_arg(args, char*));
                    break;

                case 'i':
                case 'd':
                    _x86_dbg_puts(kitoa(va_arg(args, int32_t), buf, 10));
                    break;

                case 'I':
                case 'D':
                    _x86_dbg_puts(klitoa(va_arg(args, int64_t), buf, 10));
                    break;

                case 'u':
                    _x86_dbg_puts(kutoa(va_arg(args, uint32_t), buf, 10));
                    break;

                case 'U':
                    _x86_dbg_puts(klutoa(va_arg(args, uint64_t), buf, 10));
                    break;

                case 'x':
                    _x86_dbg_puts(kutoa(va_arg(args, uint32_t), buf, 16));
                    break;

                case 'y':
                    _x86_dbg_puts(klutoa(va_arg(args, uint64_t), buf, 16));
                    break;

                case 'X':
                    _x86_dbg_puts(kstrtoupper(kutoa(va_arg(args, uint32_t), buf, 16)));
                    break;

                case 'Y':
                    _x86_dbg_puts(kstrtoupper(klutoa(va_arg(args, uint64_t), buf, 16)));
                    break;
            }
        }
        else {
            _x86_dbg_putchar(*fmt);
        }

        fmt++;
    }

    va_end(args);
}

static uint32_t kmem_physBase;
static uint32_t kmem_physUsableBase;
static size_t kmem_physSize;
static uint32_t *kmem_physStack;
static size_t kmem_pageSize;

void arch_mem_physInit(uint32_t pBase, size_t pSize, size_t pPageSize) {
    arch_dbg_printf("Physical memory manager - initialising...\n");

    // Set our basic parameters from the inputs
    kmem_physBase = pBase;
    kmem_physSize = pSize;
    kmem_pageSize = pPageSize;

    // TODO: fix base if not aligned to page size
    // TODO: probably do some bounds and sanity checking etc.

    // Calculate and reserve space for free page stack
    size_t totalPages = kmem_physSize / kmem_pageSize;
    size_t stackPages = (totalPages * 4);   // We need 4 bytes on the stack for each page that is able to be allocated
    if(stackPages % kmem_pageSize != 0) {
        stackPages = (stackPages / kmem_pageSize) + 1;
    }
    else {
        stackPages = stackPages / kmem_pageSize;
    }
    size_t stackMaxSize = stackPages * kmem_pageSize;

    // Set usable base to next page size aligned address after the stack
    kmem_physUsableBase = kmem_physBase + (stackPages * kmem_pageSize);

    // Initialise the stack
    kmem_physStack = (uint32_t*)kmem_physBase;
    for(uint32_t i = 0; i < (totalPages - stackPages); i++, kmem_physStack++) {
        *kmem_physStack = i;
    }

    // Log some info for debug purposes
    arch_dbg_printf(" - page size: %u bytes\n - base: 0x%x\n - usable base: 0x%x\n - total size: %u bytes / %u pages\n",
                    kmem_pageSize, kmem_physBase, kmem_physUsableBase, kmem_physSize, totalPages);
    arch_dbg_printf(" - stack size: %u bytes / %u pages\n - available size: %u bytes / %u pages\n",
                    stackMaxSize, stackPages, kmem_physSize - stackMaxSize, totalPages - stackPages);
    arch_dbg_printf("Physical memory manager - ready\n");
}

size_t arch_mem_getPageSize() {
    return kmem_pageSize;
}

void *arch_mem_physAlloc() {
    // If stack is at base, we're out of physical memory
    if((kmem_physStack - 1) == (uint32_t*)kmem_physBase) {
        arch_dbg_printf("ERROR: out of memory, physical memory manager has no more free blocks, allocation failed\n");
        return NULL;
    }

    // Grab index of next available frame
    kmem_physStack--;
    uint32_t frameIdx = *kmem_physStack;

    // Calculate frame address
    uint8_t *ptr = (uint8_t*)kmem_physUsableBase;
    ptr += (frameIdx * kmem_pageSize);

    arch_dbg_printf("Physical memory manager - allocated block %u\n", frameIdx);

    return (void*)ptr;
}

void arch_mem_physFree(void *pBase) {
    // Sanity check pBase is within our usable range of memory
    if(pBase < kmem_physUsableBase || pBase > (kmem_physBase + kmem_physSize) - 1) {
        arch_dbg_printf("ERROR: attempted to free block containing address 0x%x in area of memory not controlled by the physical memory manager\n", pBase);
        return;
    }

    // Figure out which frame pBase references
    uint32_t frameIdx = (((uint32_t)pBase - kmem_physUsableBase) / kmem_pageSize);

    // Sanity check we are not freeing a block that is already free (so would end up on stack twice and get double allocated)
    uint32_t *ptr = (uint32_t*)kmem_physBase;
    for(int i = 0; i < (kmem_physStack - 1); i++, ptr++) {
        if(*ptr == frameIdx) {
            arch_dbg_printf("ERROR: attempted to free block %u that is already free in physical memory manager\n", frameIdx);
            return;
        }
    }

    // Put it back on the stack
    *kmem_physStack = frameIdx;
    kmem_physStack++;

    arch_dbg_printf("Physical memory manager - freed block %u\n", frameIdx);
}

void arch_process_setInitialState(process *pProcess) {
    // allocate/init arch data area
    // set up paging tables, other descriptor tables whatever
    // set initial values for all registers
}

void arch_process_mapPage(process *pProcess, void *pPhysBase, void *pVirtBase) {
    // if the addresses aren't aligned, we're out
    // if page already mapped free it
    // set new page
    // if process is active flush cache somehow? will this even happen? not atm anyway...
}

void arch_process_setEntryPoint(process *pProcess, uint32_t pEntryPoint) {
    // Set EIP
    //process->archData->eip = pEntryPoint;
}

void arch_process_switch(process *pCurrent, process *pNew) {

}

extern void _x86_tick();

void arch_tick() {
    _x86_tick();
}
