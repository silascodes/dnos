
#include <stddef.h>
#include "kernel/multiboot.h"
#include "kernel/arch.h"
#include "kernel/kernel.h"
#include "kernel/kstring.h"
#include "kernel/term.h"
#include "kernel/kstdio.h"

extern void _enableInterrupts();
extern void _disableInterrupts();

static inline void outb(uint16_t port, uint8_t val)
{
    __asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
    /* TODO: Is it wrong to use 'N' for the port? It's not a 8-bit constant. */
    /* TODO: Should %1 be %w1? */
}

static inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    __asm volatile ( "inb %1, %0" : "=a"(ret) : "Nd"(port) );
    /* TODO: Is it wrong to use 'N' for the port? It's not a 8-bit constant. */
    /* TODO: Should %1 be %w1? */
    return ret;
}

static inline void io_wait(void)
{
    /* TODO: This is probably fragile. */
    __asm volatile ( "jmp 1f\n\t"
                   "1:jmp 2f\n\t"
                   "2:" );
}

void _x86_enableInterrupts() {
    arch_dbg_printf("Interrupts enabled\n");
    _enableInterrupts();
}

void _x86_disableInterrupts() {
    arch_dbg_printf("Interrupts disabled\n");
    _disableInterrupts();
}



// ----------
// Early debug output via serial port COM1
// ----------

// TODO: the whole thing will probably fall over badly if youre writing in data faster than it
//       can be flushed out of the buffer to the serial port (buffer size could be increased)
//       Or this could be re-written using a linked list of blocks of memory that can grow and shrink or something

#define COM1_PORT           0x3f8         // Base port for serial port COM1
#define DBG_BUFFER_SIZE     (1024 * 16)   // Size of the debug output buffer

char _x86_dbg_buffer[DBG_BUFFER_SIZE];    // Ring buffer for debug output
int _x86_dbg_buffer_next_write;
int _x86_dbg_buffer_next_read;

void _x86_dbg_init() {
    // Initialise the serial port COM1 for writing debug output
    outb(COM1_PORT + 1, 0x00);    // Disable all interrupts
    outb(COM1_PORT + 3, 0x80);    // Enable DLAB (set baud rate divisor)
    outb(COM1_PORT + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
    outb(COM1_PORT + 1, 0x00);    //                  (hi byte)
    outb(COM1_PORT + 3, 0x03);    // 8 bits, no parity, one stop bit
    outb(COM1_PORT + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
    outb(COM1_PORT + 4, 0x0B);    // IRQs enabled, RTS/DSR set

    // Initialise the Buffer
    kmemset(_x86_dbg_buffer, 0, DBG_BUFFER_SIZE);
    _x86_dbg_buffer_next_write = 0;
    _x86_dbg_buffer_next_read = 0;
}

void _x86_dbg_write() {
    // If there is nothing to write, just return
    if(_x86_dbg_buffer[_x86_dbg_buffer_next_read] == 0) {
        return;
    }

    // If the serial port is willing to let us write, lets write
    if(inb(COM1_PORT + 5) & 0x20) {
        // Write the next character (TODO: we could probably try write a few or something to speed flushing the buffer)
        outb(COM1_PORT, _x86_dbg_buffer[_x86_dbg_buffer_next_read]);

        // Clear the byte in the buffer we just wrote to the serial port
        _x86_dbg_buffer[_x86_dbg_buffer_next_read] = 0;

        // Increment our read position
        _x86_dbg_buffer_next_read++;

        // Wrap to the start of the buffer once we read the end
        if(_x86_dbg_buffer_next_read == DBG_BUFFER_SIZE) {
            _x86_dbg_buffer_next_read = 0;
        }
    }
}

void _x86_dbg_putchar(const char pChar) {
    _x86_dbg_buffer[_x86_dbg_buffer_next_write] = pChar;
    _x86_dbg_buffer_next_write++;

    // Wrap to the start of the buffer once we read the end
    if(_x86_dbg_buffer_next_write == DBG_BUFFER_SIZE) {
        _x86_dbg_buffer_next_write = 0;
    }

    // Force the buffer to flush (hopefully)
    _x86_dbg_write();
}

void _x86_dbg_puts(const char *pStr) {
    int len = kstrlen(pStr);

    // Done by calling _x86_dbg_putchar in a loop so it wraps correctly rather than using kmemcpy
    for(int i = 0; i < len; i++) {
        _x86_dbg_putchar(pStr[i]);
    }
}




// ----------
// GDT
// ----------

static uint8_t *kgdt_table;

typedef struct {
    uint16_t prevTask;
    uint16_t reserved1;
    uint32_t esp0;
    uint16_t ss0;
    uint16_t reserved2;
    uint32_t esp1;
    uint16_t ss1;
    uint16_t reserved3;
    uint32_t esp2;
    uint16_t ss2;
    uint16_t reserved4;
    uint32_t cr3;
    uint32_t eip;
    uint32_t eflags;
    uint32_t eax;
    uint32_t ecx;
    uint32_t edx;
    uint32_t ebx;
    uint32_t esp;
    uint32_t ebp;
    uint32_t esi;
    uint32_t edi;
    uint16_t es;
    uint16_t reserved5;
    uint16_t cs;
    uint16_t reserved6;
    uint16_t ss;
    uint16_t reserved7;
    uint16_t ds;
    uint16_t reserved8;
    uint16_t fs;
    uint16_t reserved9;
    uint16_t gs;
    uint16_t reserved10;
    uint16_t ldt;
    uint16_t reserved11;
    uint16_t reserved12; // The first bit of this is actually the "debug trap" flag
    uint16_t ioMapBase;
} x86_tss;
static x86_tss ktss;

extern void _loadGDT(void *pTable, uint16_t pSize);
extern void _reloadSegs();
extern void _loadTR();

void _x86_tss_init() {
    kmemset(&ktss, 0, sizeof(ktss));
    ktss.ioMapBase = sizeof(ktss);      // Apparently set this to size if it doesnt exist? Check this maybe...
    ktss.ss0 = 0x10;                    // Kernel data segment
    // TODO: we also need to set ktss.esp0 to kernel stack? not sure
}

uint64_t _x86_gdt_encode(uint32_t base, uint32_t limit, uint16_t flag) {
    uint64_t descriptor;

    // Create the high 32 bit segment
    descriptor  =  limit       & 0x000F0000;         // set limit bits 19:16
    descriptor |= (flag <<  8) & 0x00F0FF00;         // set type, p, dpl, s, g, d/b, l and avl fields
    descriptor |= (base >> 16) & 0x000000FF;         // set base bits 23:16
    descriptor |=  base        & 0xFF000000;         // set base bits 31:24

    // Shift by 32 to allow for low part of segment
    descriptor <<= 32;

    // Create the low 32 bit segment
    descriptor |= base  << 16;                       // set base bits 15:0
    descriptor |= limit  & 0x0000FFFF;               // set limit bits 15:0

    return descriptor;
}

void _x86_gdt_init() {
    arch_dbg_printf("GDT initialising...\n");

    // One physical frame allows us 512 entries, should be enough for now
    kgdt_table = (uint8_t*)arch_mem_physAlloc();

    // Initialise table to 0 (this also will setup null descriptor)
    kmemset(kgdt_table, 0, 4096);

    // Setup our basic flat model entries
    uint64_t *gdt = (uint64_t*)kgdt_table;
    gdt++;
    *gdt = _x86_gdt_encode(0, 0xFFFFFFFF, 0xC09A);        // Code segment
    gdt++;
    *gdt = _x86_gdt_encode(0, 0xFFFFFFFF, 0xC092);        // Data segment
    gdt++;
    _x86_tss_init();
    uint32_t tssBase = (uint32_t)&ktss;
    *gdt = _x86_gdt_encode(tssBase, tssBase + sizeof(ktss), 0x4089);     // TSS

    // Load the GDT
    _loadGDT(kgdt_table, 32); // TODO: Size - 1? Intel manual says 8n-1 but bootloader is 32 with 4 entires? also actually has 4096 bytes at this point... pass small val for now
    _reloadSegs();

    _loadTR();

    arch_dbg_printf("GDT ready\n");
}



// ----------
// IDT
// ----------

static uint8_t *kidt_table;

extern void _isr0();
extern void _isr1();
extern void _isr2();
extern void _isr3();
extern void _isr4();
extern void _isr5();
extern void _isr6();
extern void _isr7();
extern void _isr8();
extern void _isr9();
extern void _isr10();
extern void _isr11();
extern void _isr12();
extern void _isr13();
extern void _isr14();
extern void _isr15();
extern void _isr16();
extern void _isr17();
extern void _isr18();
extern void _isr19();
extern void _isr20();

extern void _irq0();
extern void _irq1();
extern void _irq2();
extern void _irq3();
extern void _irq4();
extern void _irq5();
extern void _irq6();
extern void _irq7();
extern void _irq8();
extern void _irq9();
extern void _irq10();
extern void _irq11();
extern void _irq12();
extern void _irq13();
extern void _irq14();
extern void _irq15();

extern void _loadIDT(void *pTable, uint16_t pSize);

#define PIC1_CMD    0x20
#define PIC1_DATA   0x21
#define PIC2_CMD    0xA0
#define PIC2_DATA   0xA1

void _x86_pic_disable() {
    arch_dbg_printf("PIC masking all lines\n");

    outb(PIC1_DATA, 0xFF);
    outb(PIC2_DATA, 0xFF);
}

void _x86_pic_init() {
    arch_dbg_printf("PIC (master + slave) initialising...\n");

    uint8_t mask1, mask2;

    mask1 = inb(PIC1_DATA);
    mask2 = inb(PIC2_DATA);

    outb(PIC1_CMD, 0x11);
    io_wait();
    outb(PIC2_CMD, 0x11);
    io_wait();

    outb(PIC1_DATA, 32);
    io_wait();
    outb(PIC2_DATA, 40);
    io_wait();

    outb(PIC1_DATA, 0x4);
    io_wait();
    outb(PIC2_DATA, 0x2);
    io_wait();

    outb(PIC1_DATA, 1);
    io_wait();
    outb(PIC2_DATA, 1);
    io_wait();

    outb(PIC1_DATA, mask1);
    outb(PIC2_DATA, mask2);

    arch_dbg_printf("PIC (master + slave) ready\n");
}

void _x86_pic_setMask(unsigned char IRQline) {
    uint16_t port;
    uint8_t value;

    arch_dbg_printf("PIC masking line %u\n", IRQline);

    if(IRQline < 8) {
        port = PIC1_DATA;
    } else if(IRQline < 16) {
        port = PIC2_DATA;
        IRQline -= 8;
    }
    else {
        arch_dbg_printf("ERROR: attempted to unmask non existant PIC line %u\n", IRQline);
        return;
    }

    value = inb(port) | (1 << IRQline);
    outb(port, value);
}

void _x86_pic_clearMask(unsigned char IRQline) {
    uint16_t port;
    uint8_t value;

    arch_dbg_printf("PIC unmasking line %u\n", IRQline);

    if(IRQline < 8) {
        port = PIC1_DATA;
    } else if(IRQline < 16) {
        port = PIC2_DATA;
        IRQline -= 8;
    }
    else {
        arch_dbg_printf("ERROR: attempted to unmask non existant PIC line %u\n", IRQline);
        return;
    }

    value = inb(port) & ~(1 << IRQline);
    outb(port, value);
}

uint64_t _x86_idt_encode(uint32_t pOffset, uint16_t pSelector) {
    uint64_t descriptor = 0;

    descriptor |= (pOffset & 0xFFFF0000);       // Offset high
    descriptor |= 0x00008E00;                   // Type, present (this value means present 32bit interrupt gate)

    descriptor <<= 32;

    descriptor |= ((uint32_t)pSelector << 16);  // Selector
    descriptor |= (pOffset & 0x0000FFFF);       // Offset low

    return descriptor;
}

void _x86_idt_init() {
    arch_dbg_printf("IDT initialising...\n");

    kidt_table = (uint8_t*)arch_mem_physAlloc();

    kmemset(kidt_table, 0, 4096);

    uint64_t *ptr = (uint64_t*)kidt_table;

    // NOTE: all reserved interrupts are mapped to the first reserved interrupt (15)
    ptr[0] = _x86_idt_encode((uint32_t)&_isr0, 0x08);
    ptr[1] = _x86_idt_encode((uint32_t)&_isr1, 0x08);
    ptr[2] = _x86_idt_encode((uint32_t)&_isr2, 0x08);
    ptr[3] = _x86_idt_encode((uint32_t)&_isr3, 0x08);
    ptr[4] = _x86_idt_encode((uint32_t)&_isr4, 0x08);
    ptr[5] = _x86_idt_encode((uint32_t)&_isr5, 0x08);
    ptr[6] = _x86_idt_encode((uint32_t)&_isr6, 0x08);
    ptr[7] = _x86_idt_encode((uint32_t)&_isr7, 0x08);
    ptr[8] = _x86_idt_encode((uint32_t)&_isr8, 0x08);
    ptr[9] = _x86_idt_encode((uint32_t)&_isr9, 0x08);
    ptr[10] = _x86_idt_encode((uint32_t)&_isr10, 0x08);
    ptr[11] = _x86_idt_encode((uint32_t)&_isr11, 0x08);
    ptr[12] = _x86_idt_encode((uint32_t)&_isr12, 0x08);
    ptr[13] = _x86_idt_encode((uint32_t)&_isr13, 0x08);
    ptr[14] = _x86_idt_encode((uint32_t)&_isr14, 0x08);
    ptr[15] = _x86_idt_encode((uint32_t)&_isr15, 0x08);
    ptr[16] = _x86_idt_encode((uint32_t)&_isr16, 0x08);
    ptr[17] = _x86_idt_encode((uint32_t)&_isr17, 0x08);
    ptr[18] = _x86_idt_encode((uint32_t)&_isr18, 0x08);
    ptr[19] = _x86_idt_encode((uint32_t)&_isr19, 0x08);
    ptr[20] = _x86_idt_encode((uint32_t)&_isr20, 0x08);
    ptr[21] = _x86_idt_encode((uint32_t)&_isr15, 0x08);
    ptr[22] = _x86_idt_encode((uint32_t)&_isr15, 0x08);
    ptr[23] = _x86_idt_encode((uint32_t)&_isr15, 0x08);
    ptr[24] = _x86_idt_encode((uint32_t)&_isr15, 0x08);
    ptr[25] = _x86_idt_encode((uint32_t)&_isr15, 0x08);
    ptr[26] = _x86_idt_encode((uint32_t)&_isr15, 0x08);
    ptr[27] = _x86_idt_encode((uint32_t)&_isr15, 0x08);
    ptr[28] = _x86_idt_encode((uint32_t)&_isr15, 0x08);
    ptr[29] = _x86_idt_encode((uint32_t)&_isr15, 0x08);
    ptr[30] = _x86_idt_encode((uint32_t)&_isr15, 0x08);
    ptr[31] = _x86_idt_encode((uint32_t)&_isr15, 0x08);

    ptr[32] = _x86_idt_encode((uint32_t)&_irq0, 0x08);
    ptr[33] = _x86_idt_encode((uint32_t)&_irq1, 0x08);
    ptr[34] = _x86_idt_encode((uint32_t)&_irq2, 0x08);
    ptr[35] = _x86_idt_encode((uint32_t)&_irq3, 0x08);
    ptr[36] = _x86_idt_encode((uint32_t)&_irq4, 0x08);
    ptr[37] = _x86_idt_encode((uint32_t)&_irq5, 0x08);
    ptr[38] = _x86_idt_encode((uint32_t)&_irq6, 0x08);
    ptr[39] = _x86_idt_encode((uint32_t)&_irq7, 0x08);
    ptr[40] = _x86_idt_encode((uint32_t)&_irq8, 0x08);
    ptr[41] = _x86_idt_encode((uint32_t)&_irq9, 0x08);
    ptr[42] = _x86_idt_encode((uint32_t)&_irq10, 0x08);
    ptr[43] = _x86_idt_encode((uint32_t)&_irq11, 0x08);
    ptr[44] = _x86_idt_encode((uint32_t)&_irq12, 0x08);
    ptr[45] = _x86_idt_encode((uint32_t)&_irq13, 0x08);
    ptr[46] = _x86_idt_encode((uint32_t)&_irq14, 0x08);
    ptr[47] = _x86_idt_encode((uint32_t)&_irq15, 0x80);

    _loadIDT(kidt_table, 4096);

    _x86_pic_init();        // Initialise the master and slave PICs
    _x86_pic_disable();     // Disable all IRQs
    _x86_pic_clearMask(0);  // Enable timer IRQ
    _x86_pic_clearMask(1);  // Enable keyboard IRQ

    arch_dbg_printf("IDT ready\n");
}

void _x86_irq_handler(uint32_t pIRQ) {
    kprintf("IRQ %d\n", pIRQ);
}



// ----------
// Paging
// ----------

static uint32_t *_x86_paging_pdt;

extern void _x86_enablePaging(uint32_t *pPageDirectory);

void _x86_paging_init() {
    // Create PDT for kernel
    _x86_paging_pdt = arch_mem_physAlloc();

    // Create PDEs for kernel (identity map whole physical space)
    uint32_t *pde;
    for(int i = 0; i < 1024; i++) {
        pde = arch_mem_physAlloc();

        // Create PDT entry
        _x86_paging_pdt[i] = (uint32_t)pde | 0x00000003;

        // Create PDE entries identity mapping this to physical addresses
        uint32_t physAddr = 0;
        for(int j = 0; j < 1024; j++) {
            pde[j] = physAddr | 0x00000003;
            physAddr += 4096;
        }
    }

    // Enable 32 bit paging by setting CR0.PG to 1 and loading CR3
    _x86_enablePaging(_x86_paging_pdt);
}



// ----------
// Memory info
// ----------

struct _x86_mem_region {
    uint64_t baseAddr;
    uint64_t size;
};

struct mmap_entry* _x86_checkMultibootMemoryMap(struct multiboot_info *pBootInfo) {
    arch_dbg_printf("Checking for memory information in multiboot info...\n");

    // mem_* fields present?
    if(pBootInfo->flags & (1 << 0)) {
        //kprintf("mem_lower: %u kB\nmem_upper: %u kB\n", pBootInfo->mem_lower, pBootInfo->mem_upper);
        arch_dbg_printf(" - mem_lower: %u kB\n - mem_upper: %u kB\n", pBootInfo->mem_lower, pBootInfo->mem_upper);
    }

    // mmap_* present?
    if(pBootInfo->flags & (1 << 6)) {
        arch_dbg_printf(" - mmap_length: %u\n - mmap_addr: %x\n", pBootInfo->mmap_length, pBootInfo->mmap_addr);

        struct mmap_entry *entry;
        uint32_t size;
        uint8_t *ptr = (uint8_t*)pBootInfo->mmap_addr;
        struct mmap_entry *best = NULL;
        arch_dbg_printf(" - mmap entries (base address, size, type):\n");
        while(ptr < ((uint8_t*)pBootInfo->mmap_addr + pBootInfo->mmap_length)) {
            // Grab pointer to the next entry then advance
            size = *ptr;
            ptr += 4;
            entry = (struct mmap_entry*)ptr;
            ptr += size;

            // Find the largest available contiguous block of RAM (according to the multiboot info)
            if(best == NULL || (entry->length > best->length && entry->type == 1)) {
                best = entry;
            }

            arch_dbg_printf("\t0x%y, %U bytes, %u %s\n", entry->base_addr, entry->length, entry->type, (entry->type == 1 ? "(available)" : ""));
        }

        return best;
    }

    return NULL;
}

void _x86_getBestMemoryRegion(struct multiboot_info *pBootInfo, struct _x86_mem_region *pRegion) {
    // First attempt to find the best memory region using the multiboot info (if present)
    struct mmap_entry *mmap = _x86_checkMultibootMemoryMap(pBootInfo);
    if(mmap != NULL) {
        pRegion->baseAddr = mmap->base_addr;
        pRegion->size = mmap->length;

        arch_dbg_printf("Found best memory region using multiboot info\n");

        // TODO: hack to not use any memory below 2mB cause overwrite ourselves (we are loaded at 1mB IIRC)
        if(pRegion->baseAddr < 0x200000) {
            size_t diff = 0x200000 - pRegion->baseAddr;
            pRegion->baseAddr = 0x200000;
            pRegion->size -= diff;
        }

        return;
    }

    // Shitty default to 4mB 2mB in (ew)
    arch_dbg_printf("Falling back to a shitty default of 4mB of memory 2mB in (ew lol)\n");
    pRegion->baseAddr = 0x200000;
    pRegion->size = 0x400000;

    return;
}



// ----------
// x86 initialisation entry point
// ----------

extern void arch_mem_physInit(uint32_t pBase, size_t pSize, size_t pPageSize);

void _x86_init(uint32_t pBootMagic, struct multiboot_info *pBootInfo) {
    // Initialise serial port debug logging
    _x86_dbg_init();
    arch_dbg_printf("Serial port (COM1) debug logging ready\n");

    // Initialise basic terminal output
    kterm_init();

    // Check multiboot magic
    if(pBootMagic != 0x2BADB002) {
        // Fuck it, let's warn and try, what could possibly go wrong...
        kprintf("WARNING: multiboot signature not found, only multiboot compliant bootloaders are supported, reluctantly continuing\n");
        arch_dbg_printf("WARNING: multiboot signature not found, only multiboot compliant bootloaders are supported, reluctantly continuing\n");
    }

    // Get memory map (our OS is shitty and the physical allocator only supports one contiguous region of memory ATM, so find the biggest)
    struct _x86_mem_region bestMemRegion;
    _x86_getBestMemoryRegion(pBootInfo, &bestMemRegion);
    arch_dbg_printf("Using memory region with base address 0x%y and size of %u bytes\n", bestMemRegion.baseAddr, bestMemRegion.size);
    arch_mem_physInit(bestMemRegion.baseAddr, bestMemRegion.size, 4096);

    // Setup GDT
    _x86_gdt_init();

    // Setup IDT
    _x86_idt_init();
    _x86_enableInterrupts();

    // Setup paging
    //_x86_paging_init();

    // Call kernel
    arch_dbg_printf("x86 initialisation completed - calling kernel main\n");
    kernel_main(pBootInfo);
}

void _x86_tick() {
    _x86_dbg_write();
}
